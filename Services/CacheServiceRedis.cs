﻿using StackExchange.Redis;
using System.IO;
using System.Runtime.Serialization;

namespace Services
{
	public class CacheServiceRedis : ICacheService
	{
		private const string ENDPOINT = "localhost";

		private readonly ConnectionMultiplexer _redisConnection;
		private readonly DataContractSerializer _serializer;

		public CacheServiceRedis()
		{
			_redisConnection = ConnectionMultiplexer.Connect(
				new ConfigurationOptions
				{
					AbortOnConnectFail = false,
					EndPoints = { ENDPOINT }
				}
				);
			_serializer = new DataContractSerializer(typeof(object));
		}

		public T GetValue<T>(string key) where T : class
		{
			var database = _redisConnection.GetDatabase();
			byte[] value = database.StringGet(key);
			if (value == null)
			{
				return default;
			}
			using (var stream = new MemoryStream(value))
			{
				return _serializer.ReadObject(stream) as T;
			}
		}

		public void SetValue<T>(string key, T value)
		{
			var database = _redisConnection.GetDatabase();

			if (value == null)
			{
				database.StringSet(key, RedisValue.Null);
			}
			else
			{
				using (var stream = new MemoryStream())
				{
					_serializer.WriteObject(stream, value);
					database.StringSet(key, stream.ToArray());
				}
			}
		}
	}
}