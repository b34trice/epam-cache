﻿namespace Services
{
	public interface ICacheService
	{
		T GetValue<T>(string key) where T : class;

		void SetValue<T>(string key, T value);
	}
}
