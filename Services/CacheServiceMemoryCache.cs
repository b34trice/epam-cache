﻿using System.Runtime.Caching;

namespace Services
{
	public class CacheServiceMemoryCache : ICacheService
	{
		private readonly MemoryCache _cache = MemoryCache.Default;

		public T GetValue<T>(string key) where T : class
		{
			return _cache[key] as T;
		}

		public void SetValue<T>(string key, T value)
		{
			_cache.Set(value.ToString(), value, new CacheItemPolicy());
		}
	}
}
