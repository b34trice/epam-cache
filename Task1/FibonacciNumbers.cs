﻿using Services;

namespace Task1
{
    public class FibonacciNumbers
    {
        private readonly ICacheService _cacheService;

        public FibonacciNumbers(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        public int Calculate(int number)
        {
            int result = number == 0 || number == 1 ? number : Calculate(number - 1) + Calculate(number - 2);
            return UpdateCacheNumber(result, _cacheService);
        }

        private int UpdateCacheNumber(int number, ICacheService cacheService)
        {
            var cacheNumber = cacheService.GetValue<object>(number.ToString());
            if (cacheNumber == null)
            {
                cacheNumber = number;
                cacheService.SetValue(number.ToString(), cacheNumber);
                return number;
            }
            else
            {
                return (int)cacheNumber;
            }
        }
    }
}