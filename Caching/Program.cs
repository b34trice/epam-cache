﻿using Services;
using System;
using Task1;

namespace Caching
{
	class Program
	{
		public static void Main(string[] args)
		{
			Task1V1(new CacheServiceMemoryCache());
			Task1V1(new CacheServiceRedis());
			var t = 0;
		}

		private static void Task1V1(ICacheService service)
		{
			var fibonacci = new FibonacciNumbers(service);
			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(fibonacci.Calculate(i));
			}
		}
	}
}
