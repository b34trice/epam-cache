﻿using NorthwindLibrary;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CachingSolutionsSamples
{
	public class CategoriesManager
	{
		private ICacheService _cache;
		private readonly string _key;

		public CategoriesManager(ICacheService cache)
		{
			_cache = cache;
			_key = Thread.CurrentPrincipal.Identity.Name;
		}

		public IEnumerable<Category> GetCategories()
		{
			Console.WriteLine("Get Categories");
			var categories = GetCache<Category>();
			if (categories == null)
			{
				Console.WriteLine("From DB");

				using (var dbContext = new Northwind())
				{
					dbContext.Configuration.LazyLoadingEnabled = false;
					dbContext.Configuration.ProxyCreationEnabled = false;
					categories = dbContext.Categories.ToList();
					_cache.SetValue(_key, categories);
				}
			}

			return categories;
		}

		public IEnumerable<Order> GetOrders()
		{
			Console.WriteLine("Get Orders");
			var orders = GetCache<Order>();
			if (orders == null)
			{
				Console.WriteLine("From DB");

				using (var dbContext = new Northwind())
				{
					dbContext.Configuration.LazyLoadingEnabled = false;
					dbContext.Configuration.ProxyCreationEnabled = false;
					orders = dbContext.Orders.ToList();
					_cache.SetValue(_key, orders);
				}
			}

			return orders;
		}

		public IEnumerable<Employee> GetEmployees()
		{
			Console.WriteLine("Get Employees");
			var employees = GetCache<Employee>();
			if (employees == null)
			{
				Console.WriteLine("From DB");
				using (var dbContext = new Northwind())
				{
					dbContext.Configuration.LazyLoadingEnabled = false;
					dbContext.Configuration.ProxyCreationEnabled = false;
					employees = dbContext.Employees.ToList();
					_cache.SetValue(_key, employees);
				}
			}

			return employees;
		}

		private IEnumerable<T> GetCache<T>()
		{
			return _cache.GetValue<IEnumerable<T>>(_key);
		}
	}
}