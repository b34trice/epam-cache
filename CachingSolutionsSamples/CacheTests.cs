﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Threading;
using Services;

namespace CachingSolutionsSamples
{
	[TestClass]
	public class CacheTests
	{
		[TestMethod]
		public void MemoryCacheCategories()
		{
			var categoryManager = new CategoriesManager(new CacheServiceMemoryCache());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetCategories().Count());
				Thread.Sleep(100);
			}
		}

		[TestMethod]
		public void RedisCacheCategories()
		{
			var categoryManager = new CategoriesManager(new CacheServiceRedis());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetCategories().Count());
				Thread.Sleep(100);
			}
		}

		[TestMethod]
		public void MemoryCacheOrders()
		{
			var categoryManager = new CategoriesManager(new CacheServiceMemoryCache());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetOrders().Count());
				Thread.Sleep(100);
			}
		}

		[TestMethod]
		public void RedisCacheOrders()
		{
			var categoryManager = new CategoriesManager(new CacheServiceRedis());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetOrders().Count());
				Thread.Sleep(100);
			}
		}

		[TestMethod]
		public void MemoryCacheEmployees()
		{
			var categoryManager = new CategoriesManager(new CacheServiceMemoryCache());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetEmployees().Count());
				Thread.Sleep(100);
			}
		}

		[TestMethod]
		public void RedisCacheEmployees()
		{
			var categoryManager = new CategoriesManager(new CacheServiceRedis());

			for (var i = 0; i < 10; i++)
			{
				Console.WriteLine(categoryManager.GetEmployees().Count());
				Thread.Sleep(100);
			}
		}
	}
}